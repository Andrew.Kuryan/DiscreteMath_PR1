package sample;

import javax.swing.JFrame;
import java.util.ArrayList;
import java.util.Scanner;
import static java.lang.Math.*;

//класс для хранения результатов решения СЛАУ
class ResultSystem{
    double b[];
    int ks;
}

/*интерфейс, описывающий общую функцию для аппроксимации функции
и возвращения результата в выбранной точке*/
interface ApproxFunction{
    Double approx(int n, double[] x, double[] y, double q);
}

public class Main{

    //размеры графического окна для вывода графика функции
    static int xSize = 600, ySize = 400;
    /*количество дополнительных точек между двумя соседними известными узлами интерполяции*/
    static int point_count = 1;

    //переменная для считывания значений, введенных пользователем с клавиатуры
    static Scanner scan = new Scanner(System.in);

    public static int menu(){
        int a;
        System.out.println("\nВыберите действие: ");
        System.out.println("1 - аппроксимация функции с помощью многочлена Лагранжа;");
        System.out.println("2 - аппроксимация функции c равноотстоящими узлами с помощью многочлена Лагранжа;");
        System.out.println("3 - аппроксимация функции методом наименьших квадратов;");
        System.out.println("4 - настройки;");
        System.out.println("0 - выход");
        //получение значения из консоли
        a = scan.nextInt();
        return a;
    }

    public static void main(String[] args) {
        int c = -1;
        //пока пользователь не введет "0"
        while (c != 0){
            //запрос у пользователя желаемого действия
            c = menu();
            switch (c){
                case 1:
                    //передача ссылки на выбранную функцию
                    calc_common_method(Main::Lagran);
                    break;
                case 2:
                    calc_const_method();
                    break;
                case 3:
                    //передача ссылки на выбранную функцию
                    calc_common_method(Main::Apr_Kv);
                    break;
                case 4:
                    settings();
                    break;
                case 0:
                    //завершение работы программы
                    System.exit(0);
            }
        }
    }

    //функция для аппроксимации функции с помошью многочлена Лагранжа
    public static Double Lagran(int n, double[] x, double[] y, double q){
        int i, j;
        double l, s;

        l = 0;
        for (i=0; i<n; i++){
            s = 1;
            //расчет произведения всех (x-xj)/(xi-xj)
            for (j=0; j<n; j++) {
                if (j != i) {
                    s = s * (q - x[j]) / (x[i] - x[j]);
                }
            }
            /*добавление к сумме очередного произведения, умноженного на значение yi*/
            l = l+y[i]*s;
        }
        //если результат выражения не определен, возвращается значение null
        if (Double.isNaN(l))
            return null;
        else
            return l;
    }

    /*функция для аппроксимации функции c равноотстоящими узлами с помошью многочлена Лагранжа*/
    public static Double Lagran_const(int n, double x0, double h, double[] y, double x){
        int i, c;
        double q, mn, sum;

        //расчет числа q
        q = (x - x0) / h;
        //расчет общего множителя
        mn = 1;
        for (i=0; i<n; i++){
            mn = mn * (q-i);
        }
        mn = mn / factorial(n-1);

        //расчет суммы (-1)^(n-i) * (C/q-i) * yi
        sum = 0;
        for (i=0; i<n; i++) {
            //расчет бинома Ньютона в числителе
            c = factorial(n - 1) / (factorial(i) * factorial(n - 1 - i));
            //добавление к сумме очередного слагаемого
            sum += pow(-1, (n - i - 1)) * y[i] * c / (q - i);
        }
        //если результат выражения не определен, возвращается значение null
        if (Double.isNaN(mn * sum))
            return null;
        else {
            return mn * sum;
        }
    }

    //функция для аппроксимации функции методом наименьших квадратов
    public static Double Apr_Kv(int n, double[] x, double[] y, double q){
        //использование многочлена второй степени
        int i, j, k;
        //матрица коэффициентов системы
        double[][] a;
        //стобец свободных членов системы
        double[] b;

        a = new double[3][4];
        b = new double[3];
        //рассчет элементов матрицы коэффициентов
        for (i=0; i<3; i++){
            for (j=0; j<3; j++){
                //значение a[0][0] равно n
                if (i == 0 && j == 0)
                    a[i][j] = n;
                else {
                    //рассчет суммы xi
                    for (k = 0; k < n; k++) {
                        /*нужная степень x в сумме соответсвует сумме индекса строки и столбца*/
                        a[i][j] = a[i][j] + pow(x[k], (i + j));
                    }
                }
            }
        }
        //рассчет столбца свободных членов
        for (i=0; i<3; i++){
            for (k=0; k<n; k++){
                /*нужная степень x в сумме соответствует индексу строки*/
                b[i] = b[i] + y[k] * pow(x[k], i);
            }
        }
        //решение системы с помощью функции SIMQ
        ResultSystem ans = new ResultSystem();
        ans.b = b;
        Holets(3, a, ans);
        //подстановка полученного решения в конечную формулу многочлена
        if (ans.ks == 0)
            return ans.b[0] + ans.b[1] * q + ans.b[2] * q * q;
        else
            return null;
    }

    //метод для взаимодействия с функциями аппроксимации
    public static void calc_common_method(ApproxFunction af){
        int i, j, n;
        double[] x, y;
        //списочный массив типа Double
        ArrayList<Double> x_full, y_full;
        //временное значение функции
        Double f;
        double q, h_full, x_prev;

        //Ввод исходных данных
        System.out.print("Введите число узлов интерполяции: ");
        n = scan.nextInt();
        //инициализация массива x
        x = new double[n];
        //инициализация массива y
        y = new double[n];
        for (i = 0; i < n; i++) {
            System.out.print("x[" + (i + 1) + "] = ");
            x[i] = scan.nextDouble();
            System.out.print("y[" + (i + 1) + "] = ");
            y[i] = scan.nextDouble();
        }
        System.out.print("Введите точку для вычисления значения функции интерполяции: ");
        q = scan.nextDouble();

        System.out.println("Значение функции в точке "+q+" = "+af.approx(n, x, y, q));
        //массив для хранения расширенного набора значений аргумента
        x_full = new ArrayList<>();
        //массив для хранения расширенного набора значений функции
        y_full = new ArrayList<>();
        //предыдущее значение x
        x_prev = x[0];
        for (i=1; i < n; i++){
            /*шаг на участке между двумя соседними узлами*/
            h_full = (x[i] - x_prev) / (point_count + 1);
            for (j=0; j<=point_count; j++){
                //расчет позиции очередной промежуточной точки
                q = x_prev + h_full * j;
                f = af.approx(n, x, y, q);
                //проверка на успешное завершение функции approx
                if (f != null) {
                    y_full.add(f);
                    x_full.add(q);
                    System.out.print("x = " + q + "   ");
                    System.out.println("y = " + f);
                }
            }
            //присвоение предыдущему значению x текущего
            x_prev = x[i];
        }
        /*расчет значения функции для последней точки, не обработанной в цикле*/
        q = x[n-1];
        f = af.approx(n, x, y, q);
        //проверка на успешное завершение функции approx
        if (f != null) {
            y_full.add(f);
            x_full.add(q);
            System.out.print("x = " + q + "   ");
            System.out.println("y = " + f);
        }
        //вывод графика функции по данным из двух расширенных наборов значений
        draw_graph(x_full, y_full);
    }

    //метод для взаимодействия с функциями аппроксимации с равным шагом
    public static void calc_const_method(){
        int i, j, n;
        double[] y;
        //списочный массив типа Double
        ArrayList<Double> x_full, y_full;
        //временное значение функции
        Double f;
        double q, x0, h, h_full, x_prev;

        //Ввод исходных данных
        System.out.print("Введите число узлов интерполяции: ");
        n = scan.nextInt();
        //инициализация массива y
        y = new double[n];
        System.out.print("x[0] = ");
        x0 = scan.nextDouble();
        System.out.print("h = ");
        h = scan.nextDouble();
        for (i=0; i<n; i++){
            System.out.print("y[" + (i + 1) + "] = ");
            y[i] = scan.nextDouble();
        }
        System.out.print("Введите точку для вычисления значения функции интерполяции: ");
        q = scan.nextDouble();

        System.out.println("Значение функции в точке "+q+" = "+Lagran_const(n, x0, h, y, q));
        //массив для хранения расширенного набора значений аргумента
        x_full = new ArrayList<>();
        //массив для хранения расширенного набора значений функции
        y_full = new ArrayList<>();
        //предыдущее значение x
        x_prev = x0;
        /*шаг на участке между двумя соседними узлами*/
        h_full = h / (point_count + 1);
        for (i=1; i < n; i++){
            for (j=0; j<=point_count; j++){
                //расчет позиции очередной промежуточной точки
                q = x_prev + h_full * j;
                f = Lagran_const(n, x0, h, y, q);
                //проверка на успешное завершение функции approx
                if (f != null) {
                    y_full.add(f);
                    x_full.add(q);
                    System.out.print("x = " + q + "   ");
                    System.out.println("y = " + f);
                }
            }
            //расчет следующего значения аргумента
            x_prev = x_prev + h;
        }
        /*расчет значения функции для последней точки, не обработанной в цикле*/
        q = x_prev;
        f = Lagran_const(n, x0, h, y, q);
        //проверка на успешное завершение функции approx
        if (f != null) {
            y_full.add(f);
            x_full.add(q);
            System.out.print("x = " + q + "   ");
            System.out.println("y = " + f);
        }
        //вывод графика функции по данным из двух расширенных наборов значений
        draw_graph(x_full, y_full);
    }

    //метод для вывода графика таблично заданной функции
    public static void draw_graph(ArrayList<Double> x, ArrayList<Double> y){
        //создание графического окна
        JFrame frame = new JFrame("График функции");
        //установка размеров окна
        frame.setSize(xSize, ySize);
        //установка команды по нажатию на кнопку закрытия окна
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        FunGraph graph = new FunGraph(x, y);
        /*добавление в окно объекта класса FunGraph, наследующего от JComponent*/
        frame.add(graph);
        //установка видимости созданного окна
        frame.setVisible(true);
    }

    public static void settings(){
        int ch = -1;
        while (ch != 0) {
            System.out.println("1 - Количество промежуточных точек: "+point_count+";\n"+
                    "2 - Шаг сетки (пикселей): "+FunGraph.h+";\n"+
                    "3 - Смещение начала координат по оси x: "+FunGraph.dx+";\n"+
                    "4 - Смещение начала координат по оси y: "+FunGraph.dy+";\n" +
                    "5 - Ширина графического окна: "+xSize+";\n"+
                    "6 - Выстора графического окна: "+ySize+".\n"+
                    "(Номер нужного пункта, чтобы изменить; 0 для выхода)");
            ch = scan.nextInt();
            switch (ch) {
                case 1:
                    System.out.print("Введите новое количество промежуточных точек: ");
                    point_count = scan.nextInt();
                    break;
                case 2:
                    System.out.print("Введите новый шаг сетки (пискелей): ");
                    FunGraph.h = scan.nextInt();
                    break;
                case 3:
                    System.out.print("Введите новое смещение начала координат по оси x: ");
                    FunGraph.dx = scan.nextDouble();
                    break;
                case 4:
                    System.out.print("Введите новое смещение начала координат по оси y: ");
                    FunGraph.dy = scan.nextDouble();
                    break;
                case 5:
                    System.out.print("Введите новую ширину графического окна: ");
                    xSize = scan.nextInt();
                    break;
                case 6:
                    System.out.print("Введите новую высоту графического окна: ");
                    ySize = scan.nextInt();
                    break;
            }
        }
    }

    //функция решения СЛАУ методом Холецкого
    public static void Holets(int n, double[][] a, ResultSystem ans){
        int i, j, k;
        final double eps = 1e-21;

        char[][] ca = new char[n][n];
        char[] cb = new char[n];
        for (i=0; i<n; i++){
            for (j=0; j<n; j++){
                ca[i][j] = '0';
            }
        }
        for (i=0; i<n; i++){
            cb[i] = '0';
        }

        for (i=0; i<n; i++){
            for (j=0; j<i; j++){
                for (k=0; k<j; k++){
                    a[i][j] = a[i][j] - mult_i(ca[i][k], ca[j][k]) * a[i][k] * a[j][k];
                }
                if (a[j][j]<eps){
                    ans.ks = 1;
                    return;
                }
                a[i][j] = a[i][j] / a[j][j];
                if (ca[j][j] == 'i'){
                    ca[i][j] = 'i';
                    a[i][j] = -a[i][j];
                }
            }
            for (k=0; k<i; k++){
                a[i][i] = a[i][i] - mult_i(ca[i][k], ca[i][k]) * a[i][k] * a[i][k];
            }
            if (a[i][i] < 0){
                ca[i][i] = 'i';
                a[i][i] = abs(a[i][i]);
            }
            a[i][i] = sqrt(a[i][i]);
        }
        for (i=0; i<n; i++){
            for (j=0; j<i; j++){
                ans.b[i] = ans.b[i] - mult_i(ca[i][j], cb[j]) * a[i][j] * ans.b[j];
            }
            if (a[i][i] < eps){
                ans.ks = 1;
                return;
            }
            ans.b[i] = ans.b[i] / a[i][i];
            if (ca[i][i] == 'i'){
                cb[i] = 'i';
                ans.b[i] = -ans.b[i];
            }
        }
        for (i=n-1; i>=0; i--){
            for (j=n-1; j>i; j--){
                ans.b[i] = ans.b[i] - a[j][i] * ans.b[j];
            }
            ans.b[i] = ans.b[i] / a[i][i];
        }
    }

    //функция для перемножения чисел i
    public static int mult_i(char c1, char c2){
        if (c1 == '0' && c2 == '0'){
            return 1;
        }
        else if (c1 == 'i' && c2 == 'i'){
            return -1;
        }
        else{
            return 0;
        }
    }

    //функция для вычисления факториала числа
    public static int factorial(int number) {
        int result, factor;
        result = 1;

        for (factor = 2; factor <= number; factor++) {
            result *= factor;
        }

        return result;
    }
}