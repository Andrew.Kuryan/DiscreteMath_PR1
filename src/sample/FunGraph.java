package sample;

import javax.swing.JComponent;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.BasicStroke;
import java.awt.Color;
import java.util.ArrayList;

public class FunGraph extends JComponent {

    ArrayList<Double> x, y;
    //шаг сетки
    static int h = 20;
    /*значения смещения для начала координат(середина левого края окна)*/
    static double dx = 0, dy = Main.ySize / 2;

    /*конструктор класса: получение набора аргументов и соответствующих им значений функции*/
    FunGraph(ArrayList<Double> x, ArrayList<Double> y){
        this.x = x;
        this.y = y;
    }

    //метод отрисовки компонента
    public void paintComponent(Graphics g) {
        super.paintComponents(g);
        //приведение объекта Graphics к типу Graphics2D
        Graphics2D g2d = (Graphics2D) g;
        int i;
        double x_pr, y_pr;

        //отрисовка вертикальной составляющей сетки
        for (i = 0; i <= Main.xSize; i += h) {
            g2d.drawLine(i, 0, i, Main.ySize);
        }
        //отрисовка горизонтальной составляющей сетки
        for (i = 0; i <= Main.ySize; i += h) {
            g2d.drawLine(0, i, Main.xSize, i);
        }
        //установка толщины пера в 5 пикселей
        g2d.setStroke(new BasicStroke(5));
        //отрисовка координатных осей
        g2d.drawLine((int) dx, 0, (int) dx, Main.ySize);
        g2d.drawLine(0, (int) dy, Main.xSize, (int) dy);
        //установка толщины пера в 3 пикселя
        g2d.setStroke(new BasicStroke(3));
        //установка цвета пера на красный
        g2d.setColor(Color.RED);
        //предыдущие значения x, y
        x_pr = x.get(0);
        y_pr = y.get(0);
        for (i = 1; i < x.size(); i++) {
            //отрисовка отрезка графика от предыдущих значений x, y до xi, yi
            g2d.drawLine((int) (x_pr * h + dx), (int) (-y_pr * h + dy),
                         (int) (x.get(i) * h + dx), (int) (-y.get(i) * h + dy));
            //присвоение предыдущим значениям x, y текущих
            x_pr = x.get(i);
            y_pr = y.get(i);
        }
    }
}
